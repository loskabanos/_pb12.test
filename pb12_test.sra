HA$PBExportHeader$pb12_test.sra
$PBExportComments$Generated Application Object
forward
global type pb12_test from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global type pb12_test from application
string appname = "pb12_test"
end type
global pb12_test pb12_test

on pb12_test.create
appname="pb12_test"
message=create message
sqlca=create transaction
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on pb12_test.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;open(w_main)
end event

